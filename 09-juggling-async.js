var http = require('http');

var url = process.argv[2];
// console.log(url);


function get(regEntry, cb) {

    regNum++;
    var request = http.get(regEntry.url, function(response) {
        // console.log("status:", res.statusCode);
        response.setEncoding("utf8");
        response.on("data", function(data) {
            regEntry.payload += data;
            // console.log(data);
        });
        response.on("end", function() {
            regNum--;
            if ( regNum === 0)
                cb();
        });
    });

    request.on('error', function(err) {
        console.log("error:", err.message);
    });
    
}

var register = Array();
var regNum = 0;

function printRegister() {
    register.forEach(function(e) { console.log(e.payload); });
}

for ( var i=2; i < process.argv.length; i++ ) {
    var regEntry = { 
        url : process.argv[i], 
        payload : "",
    };
    register.push(regEntry);
    get(regEntry, printRegister);    
}


