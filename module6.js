

var fs = require('fs');
var path = require('path');


module.exports = function(dir, ext, cb) {
    // no '.' leading ext pls
    ext = '.' + ext;
    fs.readdir(dir, function(err, list) {
        if (err) 
            return cb(err);
        else {
            /*
            var resu = Array();
            list.forEach(function(f) {
                if (path.extname(f) === ext)
                    resu.push(f);
            });
            cb(null, resu);
            */
            cb(null, list.filter(function(f) {
                return path.extname(f) === ext;
            }));
        }
    });

}
