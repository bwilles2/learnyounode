var fs = require('fs');

function anzNL(s) {
    cnt = 0;
    for (var i=0; i<s.length; i++)
        if (s[i] === '\n')
            cnt++;
    return cnt;
}   

function a(fnam, callback) {
    fs.readFile(fnam, "UTF-8", function(err, data){
        if (err) throw err;
        callback(anzNL(data));
    });
}
    
function b(fnam, callback) {
    var data = fs.readFileSync(fnam, "UTF-8");
    callback(anzNL(data));
}
    
function cb(i) {
    console.log(i);    
}

var fnam = process.argv[2];
a(fnam, cb);
//b(fnam, cb);
