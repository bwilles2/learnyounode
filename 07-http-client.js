var http = require('http');

var url = process.argv[2];
// console.log(url);

var req = http.get(url, function(res) {
//    console.log("status:", res.statusCode);
    res.setEncoding("utf8");
    var payload = "";
    res.on("data", function(data) {
        payload += data;
        console.log(data);
    });
});

req.on('error', function(err) {
    console.log("error:", err.message);
});
