// console.log(process.argv);

var fs = require('fs');
var path = require('path');

var dir = process.argv[2];
var ext = "."+process.argv[3];
// console.log("ext =", ext);

fs.readdir(dir, function(err, list) {
    list.forEach(function(f) {
        // console.log(f);
        if (path.extname(f) === ext)
            console.log(f);
    });
});
