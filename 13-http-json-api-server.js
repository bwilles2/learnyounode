
var fs = require('fs');
var http = require('http');
var url = require('url');
// var S = require('string');

// http://stackoverflow.com/questions/9781218/how-to-change-node-jss-console-font-color
var chalk = require("chalk");

var traceOutput = true;
var requestCount = 0;

function pre(o) { return "<pre>"+JSON.stringify(o,undefined,4)+"</pre>"; }
function trace(s) { if (traceOutput) console.log(s.toString()); }

function qTime(query) { 
    var d = query.hasOwnProperty("iso") 
        ? new Date(query.iso) 
        : new Date();
    // console.log(d.toString());
    return d; 
}

var port = process.argv[2];
http.createServer(function (request, response) {

    // http://stackoverflow.com/questions/11961902/nodejs-http-createserver-seems-to-call-twice
    requestCount += 1;
    trace("Request #" + requestCount+": " + request.url);
    // trace(JSON.stringify(request.headers,undefined,4));

    var o = url.parse(request.url, true);
    if ( o.pathname == "" || o.pathname == "/" ) {
        var a = [
            ".../control?q[&q...], mit q = (trace=on|off) | stat | bye",
        ];
        response.end("<pre>"+a.join("\n")+"</pre>");
    }
    else if ( /^\/control/.test(o.pathname) ) {
        if ( o.query.hasOwnProperty("bye") ) {
            response.end("at your service for " + requestCount + " requests");
            // http://stackoverflow.com/questions/5263716/graceful-shutdown-of-a-node-js-http-server
            var t = (new Date()).toLocaleString();
            console.log(t + " - " + chalk.bgRed.black(" processed "+requestCount+" requests. Bye. "));
            process.exit(0);
        }
        var r = {};
        if ( o.query.hasOwnProperty("trace") ) {
            if ( o.query.trace == "on" ) {
                traceOutput = true;
                r.trace = "switched on";
            }
            if ( o.query.trace == "off" ) {
                traceOutput = false;     
                r.trace = "switched off";
            }
                       
        }
        if ( o.query.hasOwnProperty("stat") ) {
            r.requests = requestCount;
            r.trace = traceOutput ? "on" : "off";
        }
        response.end(JSON.stringify(r));
    }
    else if ( /^\/api\/parsetime/.test(o.pathname) ) {
        var d = qTime(o.query);
        response.writeHead(200, { 'Content-Type': 'application/json' })
        response.end(JSON.stringify({   
            hour : d.getHours(),
            minute : d.getMinutes(),
            second : d.getSeconds()  
        }));
    }
    else if ( /^\/api\/unixtime/.test(o.pathname) ) {
        var d = qTime(o.query);
        response.writeHead(200, { 'Content-Type': 'application/json' })
        response.end(JSON.stringify({
            unixtime : d.getTime()
        }));
    }
    else {
        response.writeHead(400, { 'Content-Type': 'application/json' })
        response.end("bad request.");
    }


    response.end(pre(o));
//      var fnam = process.argv[3];
//      var rs = fs.createReadStream(fnam)
//      rs.pipe(response)
}).listen(port);
var t = (new Date()).toLocaleString();
console.log(t + " - " + chalk.bgGreen.black(" listening on port "+port+" "));


/*
Write an HTTP server that serves JSON data when it receives a GET request to 
the path '/api/parsetime'. Expect the request to contain a query string with 
a key 'iso' and an ISO-format time as the value.

For example:

  /api/parsetime?iso=2013-08-10T12:10:15.474Z

The JSON response should contain only 'hour', 'minute' and 'second' properties. 
For example:

    {
      "hour": 14,
      "minute": 23,
      "second": 15
    }

Add second endpoint for the path '/api/unixtime' which accepts the same query 
string but returns UNIX epoch time in milliseconds (the number of milliseconds 
since 1 Jan 1970 00:00:00 UTC) under the property 'unixtime'. For example:

    { "unixtime": 1376136615474 }

Your server should listen on the port provided by the first argument to your 
program.

-------------------------------------------------------------------------------

## HINTS

The request object from an HTTP server has a url property that you will need to 
use to "route" your requests for the two endpoints.

You can parse the URL and query string using the Node core 'url' module. 
url.parse(request.url, true) will parse content of request.url and provide you 
with an object with helpful properties.

For example, on the command prompt, type:

    $ node -pe "require('url').parse('/test?q=1', true)"

Documentation on the url module can be found by pointing your browser here:
  file:///usr/lib/node_modules/learnyounode/node_apidoc/url.html

Your response should be in a JSON string format. Look at JSON.stringify() for 
more information.

You should also be a good web citizen and set the Content-Type properly:

    res.writeHead(200, { 'Content-Type': 'application/json' })

The JavaScript Date object can print dates in ISO format, e.g. 
new Date().toISOString(). It can also parse this format if you pass the string 
into the Date constructor. Date#getTime() will also come in handy.
*/
